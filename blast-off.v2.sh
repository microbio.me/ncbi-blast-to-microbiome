#!/bin/bash/

#dependencies 
#sudo apt-get install exonerate
#sudo apt-get install parallel

dir="someRandomDirectory"
outdir=$PWD/"output"
blastn="/mnt/htsd/Programmes/BLAST/ncbi-blast-2.2.29+/bin/blastn"

mkdir $dir

cores=$(grep -c ^processor /proc/cpuinfo)

echo $cores

fastasplit -c $cores -o $PWD/$dir -f $1 

output=$1'.out'

touch $output

echo 'id queryLen queryStart queryEnd alignmentLen pIdent eVal mismatches rawScore refLen taxId btop' > $output

parallel --progress 'FASTA={}; blastn -query $PWD/$dir/$FASTA -db /media/equipment/4THDD/NCBIData/global_db/global.fasta -max_target_seqs 3 -max_hsps 1 -num_threads 1 -outfmt "6 qseqid qlen qstart qend length pident evalue mismatch score slen staxids btop" -out $PWD/$FASTA".out"' ::: $dir/*

echo 'concatenating output'

for filename in $dir/*.out; do
	while read LINE; do echo $LINE >> $output; done < $filename;
done

rm -r $dir



