# Fasta to microbio.me output.

## 1. Conversion to fasta.

Input to BLAST must be in fasta format. If currently in .fastq, convert with `fasta_conversion.sh`

`bash fasta_conversion.sh input.fastq output.fasta`

## 2. BLASTING

Use the `blast-off.vX.sh` file to convert. Prior to this, check the `blast-off` script has the correct paths.

Specifically, check the db string in the `parallel` line.  

Then run with:

`bash blast-off.vX.sh fasta_file_to_blast.fasta`

## 3. Running Blast2Microbiome

Get your hands on a copy of the BLAST2Microbiome software. 

run: 

`blast2microbiome -if blast_file.out -s 2/3` 

`s` denotes the spike version
`if` is the output file from the blast script.

